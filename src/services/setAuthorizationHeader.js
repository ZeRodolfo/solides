import api from './api';

export default (token) => {
	if (token) {
		api.defaults.headers.common.authorization = `Bearer ${token}`;
	} else {
		delete api.defaults.headers.common.authorization;
	}
};
