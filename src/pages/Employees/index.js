import React, { Component, forwardRef } from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";
import MaterialTable from "material-table";
import {
  Add,
  Clear,
  Search,
  Delete,
  Edit,
  FirstPage,
  LastPage,
  ChevronRight,
  ChevronLeft,
  ArrowUpward
} from "@material-ui/icons";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import EmployeeActions from "../../store/ducks/employee";

import {
  ContentButton,
  LabelButton,
  Paper,
  ContentIcon,
  LinkIcon
} from "./styles";

import { LinkButton } from "../../styles/components";
import DeleteItemDialog from "../../components/DeleteItemDialog";

class Employees extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        { title: "Chapa", field: "chapa" },
        { title: "Nome", field: "name" },
        { title: "Permissão de Acesso", field: "role" },
        {
          title: "Ações",
          detailPanelColumnAlignment: "right",
          headerStyle: { color: "black", textAlign: "right" },
          render: rowData => (
            <ContentIcon>
              <LinkIcon to={`/employees/${rowData._id}`}>
                <Edit />
              </LinkIcon>
              <LinkIcon
                to="/"
                onClick={e =>
                  this.handleDialogOpen(e, `/employees/${rowData._id}`)
                }
              >
                <Delete />
              </LinkIcon>
            </ContentIcon>
          )
        }
      ],
      openDialog: false,
      destroyItem: null
    };

    this.handleDestroyItem = this.handleDestroyItem.bind(this);
    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  static propTypes = {
    listEmployeesRequest: PropTypes.func.isRequired,
		destroyEmployeeRequest: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const { listEmployeesRequest } = this.props;
    listEmployeesRequest();
  };

  handleDialogOpen(e, link) {
    e.preventDefault();

    this.setState({ openDialog: true, destroyItem: link });
  }

  handleDialogClose() {
    this.setState({ openDialog: false, destroyItem: "" });
  }

  handleDestroyItem = () => {
    const { destroyItem } = this.state;
		const { destroyEmployeeRequest } = this.props;

		destroyEmployeeRequest(destroyItem);

		this.setState({ openDialog: false, destroyItem: "" });
  }

  render() {
    const tableIcons = {
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Delete {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => (
        <ChevronRight {...props} ref={ref} />
      )),
      PreviousPage: forwardRef((props, ref) => (
        <ChevronLeft {...props} ref={ref} />
      )),
      SortArrow: forwardRef((props, ref) => (
        <ArrowUpward {...props} ref={ref} />
      ))
    };

    const { data } = this.props;
    const { columns, openDialog } = this.state;

    return (
      <Grid container spacing={1}>
        <Grid item md={12} xs={12}>
          <Paper>
            <ContentButton item xs={12}>
              <LinkButton to={`/employees/new`}>
                <LabelButton>Adicionar</LabelButton> <Add />
              </LinkButton>
            </ContentButton>
            <Grid item md={12} xs={12}>
              <MaterialTable
                title="Funcionários Cadastrados"
                columns={columns}
                data={data}
                icons={tableIcons}
              />

              {openDialog && (
                <DeleteItemDialog
                  handleDestroyItem={this.handleDestroyItem}
                  handleDialogClose={this.handleDialogClose}
                />
              )}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.employee.loading,
  data: state.employee.data
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(EmployeeActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employees);
