import React, { Component, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import MaterialTable from 'material-table';
import {
	Add,
	Clear,
	Search,
	Delete,
	Edit,
	FirstPage,
	LastPage,
	ChevronRight,
	ChevronLeft,
	ArrowUpward
} from '@material-ui/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PunchActions from '../../store/ducks/punch';

import { ContentButton, LabelButton, Paper, ContentIcon, LinkIcon } from './styles';

import { LinkButton } from '../../styles/components';
import DeleteItemDialog from '../../components/DeleteItemDialog';

class Punches extends Component {
	constructor(props) {
		super(props);

		const { editable, destroy } = props;

		const columns = [
			{ title: 'Data', field: 'date' },
			{ title: 'Entrada', field: 'entry' },
			{ title: 'Saída', field: 'leave' },
			{ title: 'Intervalo do Almoço', field: 'interval' }
		];

		if (editable || destroy) {
			columns.push({
				title: 'Ações',
				detailPanelColumnAlignment: 'right',
				headerStyle: { color: 'black', textAlign: 'right' },
				render: (rowData) => (
					<ContentIcon>
						{editable && (
							<LinkIcon to={`/punches/${rowData._id}`}>
								<Edit />
							</LinkIcon>
						)}
						{destroy && (
							<LinkIcon to="/" onClick={(e) => this.handleDialogOpen(e, `/punches/${rowData._id}`)}>
								<Delete />
							</LinkIcon>
						)}
					</ContentIcon>
				)
			});
		}

		this.state = {
			columns: columns,
			openDialog: false,
			destroyItem: null
		};

		this.handleDestroyItem = this.handleDestroyItem.bind(this);
		this.handleDialogOpen = this.handleDialogOpen.bind(this);
		this.handleDialogClose = this.handleDialogClose.bind(this);
	}

	static propTypes = {
		listPunchesRequest: PropTypes.func.isRequired,
		destroyPunchRequest: PropTypes.func.isRequired,
		loading: PropTypes.bool.isRequired,
		data: PropTypes.array.isRequired,
		editable: PropTypes.bool,
		destroy: PropTypes.bool,
		create: PropTypes.bool,
		employeeId: PropTypes.string
	};

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		const { listPunchesRequest, employeeId } = this.props;
		listPunchesRequest(employeeId);
	};

	handleDialogOpen(e, link) {
		e.preventDefault();

		this.setState({ openDialog: true, destroyItem: link });
	}

	handleDialogClose() {
		this.setState({ openDialog: false, destroyItem: '' });
	}

	handleDestroyItem = () => {
		const { destroyItem } = this.state;
		const { destroyPunchRequest } = this.props;

		destroyPunchRequest(destroyItem);

		this.setState({ openDialog: false, destroyItem: '' });
	};

	render() {
		const tableIcons = {
			ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
			Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
			Delete: forwardRef((props, ref) => <Delete {...props} ref={ref} />),
			Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
			FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
			LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
			NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
			PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
			SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />)
		};

		const { data, create } = this.props;
		const { columns, openDialog } = this.state;

		return (
			<Grid container spacing={1}>
				<Grid item md={12} xs={12}>
					<Paper>
						{create && (
							<ContentButton item xs={12}>
								<LinkButton to={`/punches/new`}>
									<LabelButton>Adicionar</LabelButton> <Add />
								</LinkButton>
							</ContentButton>
						)}
						<Grid item md={12} xs={12}>
							<MaterialTable title="Pontos Cadastrados" columns={columns} data={data} icons={tableIcons} />

							{openDialog && (
								<DeleteItemDialog
									handleDestroyItem={this.handleDestroyItem}
									handleDialogClose={this.handleDialogClose}
								/>
							)}
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = (state) => ({
	loading: state.punch.loading,
	data: state.punch.data,
	employee: state.punch.employee
});

const mapDispatchToProps = (dispatch) => bindActionCreators(PunchActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Punches);
