import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, AppBar, Tabs, Tab } from '@material-ui/core';

import TabPanel from '../../components/TabPanel';
import Employee from '../../components/Employee';
import Punches from '../Punches';

class EditEmployee extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: 'employee'
		};

		this.handleChangeTab = this.handleChangeTab.bind(this);
	}

	static propTypes = {
		match: PropTypes.shape({
			params: PropTypes.shape({
				id: PropTypes.string
			})
		}).isRequired
	};

	handleChangeTab(event, value) {
		this.setState({ value });
	}

	tabProps(index) {
		return {
			id: `simple-tab-${index}`,
			'aria-controls': `simple-tabpanel-${index}`
		};
	}

	render() {
		const { value } = this.state;
		const { id } = this.props.match.params;

		return (
			<Grid container>
				<AppBar position="static">
					<Tabs value={value} onChange={this.handleChangeTab} aria-label="Tabs">
						<Tab value="employee" label="Funcionário" {...this.tabProps('employee')} />
						<Tab value="punches" label="Pontos" {...this.tabProps('punches')} />
					</Tabs>
				</AppBar>
				<TabPanel value={value} index="employee">
					<Employee {...this.props} />
				</TabPanel>
				<TabPanel value={value} index="punches">
					<Punches editable={false} destroy={false} create={false} employeeId={id} />
				</TabPanel>
			</Grid>
		);
	}
}

export default EditEmployee;
