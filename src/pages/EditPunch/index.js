import React from 'react';
import { Grid } from '@material-ui/core';
import Punch from '../../components/Punch';

export default function EditPunch(props) {
	return (
		<Grid container>
			<Punch {...props} />
		</Grid>
	);
}
