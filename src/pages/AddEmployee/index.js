import React from 'react';
import { Grid } from '@material-ui/core';
import Employee from '../../components/Employee';

export default function AddEmployee(props) {
	return (
		<Grid container>
			<Employee {...props} />
		</Grid>
	);
}
