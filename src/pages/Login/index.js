import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Grid, Typography, TextField, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthActions from '../../store/ducks/auth';

import Loading from '../../components/Loading';

class Login extends Component {
	state = {
		chapa: '',
		password: ''
	};

	static propTypes = {
		loginRequest: PropTypes.func.isRequired,
		loading: PropTypes.bool.isRequired
	};

	handleChange = (e) => this.setState({ [e.target.name]: e.target.value });

	handleSubmit = (e) => {
		e.preventDefault();
		const { loginRequest } = this.props;
		const { chapa, password } = this.state;
		loginRequest(chapa, password);
	};

	render() {
		const { chapa, password } = this.state;
		const { loading } = this.props;

		return (
			<Container component="main" maxWidth="xs">
				{loading && <Loading />}
				<Grid container>
					<Typography component="h1" variant="h5">
						Login
					</Typography>

					<form noValidate onSubmit={this.handleSubmit}>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="chapa"
							label="Chapa do Empregado"
							name="chapa"
							autoComplete="chapa"
							autoFocus
							value={chapa}
							onChange={this.handleChange}
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="password"
							label="Senha"
							type="password"
							id="password"
							autoComplete="current-password"
							value={password}
							onChange={this.handleChange}
						/>
						<Button type="submit" fullWidth variant="contained" color="primary">
							Entrar
						</Button>
					</form>
				</Grid>
			</Container>
		);
	}
}

const mapStateToProps = (state) => ({
	loading: state.auth.loading
});

const mapDispatchToProps = (dispatch) => bindActionCreators(AuthActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
