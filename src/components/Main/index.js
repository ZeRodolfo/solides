import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import useStyles from './styles';

function Main(props) {
	const classes = useStyles();
	const { open } = props;

	return (
		<main
			className={clsx(classes.content, {
				[classes.contentShift]: open
			})}
		>
			<div className={classes.drawerHeader} />
			<div>{props.children}</div>
		</main>
	);
}

Main.propTypes = {
	open: PropTypes.bool.isRequired
};

export default Main;
