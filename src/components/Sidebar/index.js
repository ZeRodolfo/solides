import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useTheme } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { IconButton, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthActions from '../../store/ducks/auth';
import MenuActions from '../../store/ducks/menu';

import useStyles from './styles';

function getRoleUser(user) {
	if (user === null) return '';
	return user.role;
}

function row(role, item) {
	if (role === 'admin' || item.role === role) {
		return (
			<ListItem key={item.name} button component={Link} to={item.route}>
				<ListItemIcon>{item.icon}</ListItemIcon>
				<ListItemText primary={item.name} />
			</ListItem>
		);
	} else {
		return null;
	}
}

function Sidebar(props) {
	const classes = useStyles();
	const theme = useTheme();
	const { open, onClick, items, role } = props;

	const list = items.map((item) => row(role, item));

	return (
		<Fragment>
			<Drawer
				className={classes.drawer}
				variant="persistent"
				anchor="left"
				open={open}
				classes={{
					paper: classes.drawerPaper
				}}
			>
				<div className={classes.drawerHeader}>
					<IconButton onClick={onClick}>
						{theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
					</IconButton>
				</div>
				<Divider />
				<List>{list}</List>
			</Drawer>
		</Fragment>
	);
}

Sidebar.propTypes = {
	open: PropTypes.bool.isRequired,
	onClick: PropTypes.func.isRequired,
	items: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string.isRequired,
			route: PropTypes.string.isRequired,
			icon: PropTypes.object.isRequired
		})
	).isRequired,
	role: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
	role: getRoleUser(state.auth.user)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...AuthActions, ...MenuActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
