import React from "react";
import PropTypes from "prop-types";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";

function DeleteItemDialog(props) {
  const { handleDestroyItem, handleDialogClose } = props;

  return (
    <Dialog
      open={true}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Você irá apagar um item</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Tem certeza que deseja apagar?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDialogClose} color="primary">
          Cancelar
        </Button>
        <Button onClick={handleDestroyItem} color="primary" autoFocus>
          Apagar
        </Button>
      </DialogActions>
    </Dialog>
  );
}

DeleteItemDialog.propTypes = {
  handleDestroyItem: PropTypes.func.isRequired,
  handleDialogClose: PropTypes.func.isRequired
};

export default DeleteItemDialog;
