import React from 'react';
import PropTypes from 'prop-types';
import { Grid, SnackbarContent, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ErrorActions from '../../store/ducks/error';
import useStyles from './styles';

function close(hide) {
	return (
		<Button color="inherit" size="small" onClick={hide}>
			Fechar
		</Button>
	);
}

function Error({ message, hide }) {
	const classes = useStyles();

	return (
		!!message && (
			<Grid container className={classes.container}>
				<div className={classes.root}>
					<SnackbarContent className={classes.snackbar} message={message} action={close(hide)} />
				</div>
			</Grid>
		)
	);
}

Error.propTypes = {
	message: PropTypes.string,
	hide: PropTypes.func.isRequired
};

Error.defaultTypes = {
	message: null
};

function getMessage(error) {
	if (!error) return null;

	const text = error.response !== undefined ? error.response.data.error : error.message;
	return text;
}

const mapStateToProps = ({ error }) => ({
	message: getMessage(error.error)
});

const mapDispatchToProps = (dispatch) => bindActionCreators(ErrorActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Error);
