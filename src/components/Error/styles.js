import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
	container: {
		display: 'flex',
		justifyContent: 'center'
	},
	root: {
		maxWidth: 600
	},
	snackbar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.error.dark
	}
}));
