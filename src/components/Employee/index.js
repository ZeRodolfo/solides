import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, InputLabel, MenuItem } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import EmployeeActions from '../../store/ducks/employee';

import { Row, Title, CircleField, ContainerForm, ContentField, TextField, Select, FormControl } from './styles';
import { LinkButton, Button } from '../../styles/components';

class Employee extends Component {
	state = {
		chapa: '',
		name: '',
		password: '',
		role: 'estagiario'
	};

	static propTypes = {
		employeeRequest: PropTypes.func.isRequired,
		fetchEmployeeRequest: PropTypes.func.isRequired,
		match: PropTypes.shape({
			params: PropTypes.shape({
				id: PropTypes.string
			})
		}),
		loading: PropTypes.bool.isRequired,
		chapa: PropTypes.string,
		name: PropTypes.string,
		password: PropTypes.string,
		role: PropTypes.string
	};

	componentDidMount() {
		this.loadEmployee();
	}

	componentDidUpdate(props) {
		const { name, chapa, password, role, loading } = this.props;

		if (props.loading && !loading) {
			this.setState({ name, chapa, password, role });
		}
	}

	loadEmployee = () => {
		const { id } = this.props.match.params;
		const { fetchEmployeeRequest } = this.props;
		fetchEmployeeRequest(id);
	};

	handleChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	loadPropsToState = (props) => {
		const { name, chapa, password, role } = props;
		this.setState({ name, chapa, password, role });
	};

	submitFormHandler = (e) => {
		e.preventDefault();

		const { employeeRequest } = this.props;
		const { name, chapa, password, role } = this.state;
		const { id } = this.props.match.params;

		employeeRequest(id, chapa, name, password, role);
	};

	render() {
		const { name, chapa, password, role } = this.state;

		return (
			<Grid container>
				<Row spacing={1}>
					<CircleField>
						<Add />
					</CircleField>
				</Row>
				<Row>
					<Title variant="h6">Cadastro do Funcionário</Title>
				</Row>
				<ContainerForm noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							required
							id="chapa-required"
							name="chapa"
							label="Chapa"
							margin="normal"
							variant="outlined"
							autoComplete="off"
							value={chapa}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							required
							id="name-required"
							name="name"
							label="Nome"
							margin="normal"
							variant="outlined"
							autoComplete="off"
							value={name}
							autoFocus={true}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							required
							id="password-required"
							name="password"
							label="Senha"
							margin="normal"
							variant="outlined"
							type="password"
							autoComplete="off"
							value={password}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<FormControl>
							<InputLabel htmlFor="role-simple">Permissão do Funcionário</InputLabel>
							<Select
								value={role}
								onChange={this.handleChange}
								inputProps={{
									name: 'role',
									id: 'role-simple'
								}}
							>
								<MenuItem value="admin">Administrador</MenuItem>
								<MenuItem value="estagiario">Estagiário</MenuItem>
							</Select>
						</FormControl>
					</ContentField>
					<Row>
						<ContentField item xs={12} sm={6}>
							<LinkButton to="/employees">Voltar</LinkButton>
						</ContentField>
						<ContentField container item xs={12} sm={6} alignItems="flex-start" justify="flex-end" direction="row">
							<Button type="submit">Salvar</Button>
						</ContentField>
					</Row>
				</ContainerForm>
			</Grid>
		);
	}
}

const mapStateToProps = (state) => ({
	loading: state.employee.loading,
	chapa: state.employee.chapa,
	name: state.employee.name,
	password: state.employee.password,
	role: state.employee.role
});

const mapDispatchToProps = (dispatch) => bindActionCreators(EmployeeActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Employee);
