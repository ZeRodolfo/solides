import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import MenuIcon from '@material-ui/icons/Menu';
import { CssBaseline, AppBar, Toolbar, IconButton, Typography, Link } from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthActions from '../../store/ducks/auth';
import MenuActions from '../../store/ducks/menu';

import useStyles from './styles';
import Sidebar from '../Sidebar';
import Main from '../Main';

function getFirstName(user) {
	if (user === null) return '';

	const [ firstName ] = user.name.split(' ');
	return firstName;
}

function Header(props) {
	const classes = useStyles();
	const [ open, setOpen ] = React.useState(false);
	const { sidebarItems, name, authenticated } = props;

	function handleDrawerOpen() {
		setOpen(true);
	}

	function handleDrawerClose() {
		setOpen(false);
	}

	function handleLogoutClick() {
		const { logout } = props;
		localStorage.removeItem('@SOLIDES:userToken');
		logout();
	}

	return (
		<Fragment>
			{authenticated ? (
				<div className={classes.root}>
					<CssBaseline />
					<AppBar position="fixed" className={clsx(classes.appBar, { [classes.appBarShift]: open })}>
						<Toolbar>
							<IconButton
								edge="start"
								className={clsx(classes.menuButton, open && classes.hide)}
								color="inherit"
								aria-label="open drawer"
								onClick={handleDrawerOpen}
							>
								<MenuIcon />
							</IconButton>
							<Typography className={classes.title} variant="h6" noWrap>
								Painel
							</Typography>

							<div className={classes.sectionDesktop}>
								<Typography variant="h6" noWrap>
									<span className={classes.welcome}>Seja bem vindo, </span>
									{name}
									<Link className={classes.link} component={RouterLink} to="/login" onClick={handleLogoutClick}>
										Sair
									</Link>
								</Typography>
							</div>
						</Toolbar>
					</AppBar>

					<Sidebar open={open} onClick={handleDrawerClose} items={sidebarItems} />
					<Main open={open}>{props.children}</Main>
				</div>
			) : (
				props.children
			)}
		</Fragment>
	);
}

Header.propTypes = {
	sidebarItems: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string.isRequired,
			route: PropTypes.string.isRequired,
			icon: PropTypes.object.isRequired
		})
	).isRequired,
	authenticated: PropTypes.bool.isRequired,
	name: PropTypes.string.isRequired,
	logout: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	authenticated: !!state.auth.user,
	name: getFirstName(state.auth.user)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...AuthActions, ...MenuActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
