import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		width: '100%'
	},
	title: {
		flexGrow: 1
	},
	appBar: {
		transition: theme.transitions.create([ 'margin', 'width' ], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px) !important`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create([ 'margin', 'width' ], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	hide: {
		display: 'none !important'
	},
	welcome: {
		fontWeight: 'normal',
		color: theme.palette.secondary.light
	},
	link: {
		color: theme.palette.secondary.main,
		marginLeft: theme.spacing(3),
		'&:hover': {
			textDecoration: 'none',
			color: theme.palette.secondary.main
		}
	}
}));

export default useStyles;
