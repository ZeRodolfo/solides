import React from 'react';
import { Grid, CircularProgress } from '@material-ui/core';

import useStyles from './styles';

function Loading() {
	const classes = useStyles();

	return (
		<Grid container className={classes.container}>
			<div className={classes.root}>
				<CircularProgress variant="indeterminate" />
			</div>
		</Grid>
	);
}

export default Loading;
