import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PunchActions from '../../store/ducks/punch';

import { Row, Title, CircleField, ContainerForm, ContentField, TextField } from './styles';
import { LinkButton, Button } from '../../styles/components';

class Punch extends Component {
	state = {
		date: '',
		entry: '',
		leave: '',
		interval: '',
		employee: null
	};

	static propTypes = {
		punchRequest: PropTypes.func.isRequired,
		fetchPunchRequest: PropTypes.func.isRequired,
		match: PropTypes.shape({
			params: PropTypes.shape({
				id: PropTypes.string
			})
		}),
		loading: PropTypes.bool.isRequired,
		date: PropTypes.string,
		entry: PropTypes.string,
		leave: PropTypes.string,
		interval: PropTypes.string,
		employee: PropTypes.any
	};

	componentDidMount() {
		this.loadPunch();
	}

	componentDidUpdate(props) {
		const { date, entry, leave, interval, employee, loading } = this.props;

		if (props.loading && !loading) {
			this.setState({ date: this.formatStringDate(date), entry, leave, interval, employee });
		}
	}

	formatStringDate(strDate) {
		return strDate.replace('T00:00:00.000Z', '');
	}

	loadPunch = () => {
		const { id } = this.props.match.params;
		const { fetchPunchRequest } = this.props;
		fetchPunchRequest(id);
	};

	handleChange = (e) => this.setState({ [e.target.name]: e.target.value });

	submitFormHandler = (e) => {
		e.preventDefault();

		const { punchRequest } = this.props;
		const { date, entry, leave, interval } = this.state;
		const { id } = this.props.match.params;

		punchRequest(id, date, entry, leave, interval);
	};

	render() {
		const { date, entry, leave, interval } = this.state;

		return (
			<Grid container>
				<Row spacing={1}>
					<CircleField>
						<Add />
					</CircleField>
				</Row>
				<Row>
					<Title variant="h6">Cadastro do Ponto</Title>
				</Row>
				<ContainerForm noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							required
							id="date-required"
							name="date"
							label="Data"
							margin="normal"
							variant="outlined"
							autoComplete="off"
							type="date"
							autoFocus={true}
							value={date}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							required
							id="entry-required"
							name="entry"
							label="Entrada"
							margin="normal"
							variant="outlined"
							type="time"
							value={entry}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							id="leave"
							name="leave"
							label="Saída"
							margin="normal"
							variant="outlined"
							type="time"
							value={leave}
							onChange={this.handleChange}
						/>
					</ContentField>
					<ContentField item xs={12} sm={6} md={3}>
						<TextField
							id="interval"
							name="interval"
							label="Intervalo do Almoço"
							margin="normal"
							variant="outlined"
							type="time"
							value={interval}
							onChange={this.handleChange}
						/>
					</ContentField>
					<Row>
						<ContentField item xs={12} sm={6}>
							<LinkButton to="/punches">Voltar</LinkButton>
						</ContentField>
						<ContentField container item xs={12} sm={6} alignItems="flex-start" justify="flex-end" direction="row">
							<Button type="submit">Salvar</Button>
						</ContentField>
					</Row>
				</ContainerForm>
			</Grid>
		);
	}
}

const mapStateToProps = (state) => ({
	loading: state.punch.loading,
	date: state.punch.date,
	entry: state.punch.entry,
	leave: state.punch.leave,
	interval: state.punch.interval,
	employee: state.punch.employee
});

const mapDispatchToProps = (dispatch) => bindActionCreators(PunchActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Punch);
