import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const UserRoute = ({ authenticated, component: Component, ...otherProps }) => (
	<Route {...otherProps} render={(props) => (authenticated ? <Component {...props} /> : <Redirect to="/login" />)} />
);

UserRoute.propTypes = {
	authenticated: PropTypes.bool.isRequired,
	component: PropTypes.any.isRequired
};

const mapStateToProps = (state) => ({
	authenticated: !!state.auth.user || state.auth.loading
});

export default connect(mapStateToProps)(UserRoute);
