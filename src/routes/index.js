import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import GuestRoute from './GuestRoute';
import UserRoute from './UserRoute';

import Login from '../pages/Login';
import AddEmployee from '../pages/AddEmployee';
import EditEmployee from '../pages/EditEmployee';
import Employees from '../pages/Employees';

import Punches from '../pages/Punches';
import AddPunch from '../pages/AddPunch';
import EditPunch from '../pages/EditPunch';

const routes = () => (
	<Switch>
		<Route exact path="/" component={() => <Redirect to="/punches" />} />
		<GuestRoute exact path="/login" component={Login} />
		<UserRoute exact path="/punches" component={() => <Punches editable={true} destroy={true} create={true} />} />
		<UserRoute exact path="/punches/new" component={AddPunch} />
		<UserRoute exact path="/punches/:id" component={EditPunch} />

		<UserRoute exact path="/employees" component={Employees} />
		<UserRoute exact path="/employees/new" component={AddEmployee} />
		<UserRoute path="/employees/:id" component={EditEmployee} />
	</Switch>
);

export default routes;
