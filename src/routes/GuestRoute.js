import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const GuestRoute = ({ isAuthenticated, component: Component, ...otherProps }) => (
	<Route
		{...otherProps}
		render={(props) => (!isAuthenticated ? <Component {...props} /> : <Redirect to="/punches" />)}
	/>
);

GuestRoute.propTypes = {
	component: PropTypes.object.isRequired,
	isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
	isAuthenticated: !!state.auth.user
});

export default connect(mapStateToProps)(GuestRoute);
