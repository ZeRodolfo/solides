import React from 'react';
import { Provider } from 'react-redux';
//import { BrowserRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import SupervisedUserCircle from '@material-ui/icons/SupervisedUserCircle';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Router } from 'react-router-dom';

import { theme } from './styles/global';
import './config/reactotron';
import store from './store';
import AuthActions from './store/ducks/auth';
import Routes from './routes';
import history from './config/history';
import Header from './components/Header';
import Error from './components/Error';

if (localStorage.getItem('@SOLIDES:userToken')) {
	store.dispatch(AuthActions.fetchUserRequest());
}

function App() {
	const sidebarItems = [
		{
			name: 'Pontos',
			route: '/punches',
			icon: <AccessTimeIcon />,
			role: 'estagiario'
		},
		{
			name: 'Funcionários',
			route: '/employees',
			icon: <SupervisedUserCircle />,
			role: 'admin'
		}
	];

	return (
		<Provider store={store}>
			<Router history={history}>
				<MuiThemeProvider theme={theme}>
					<Grid container>
						<Header sidebarItems={sidebarItems}>
							<Error />
							<Routes />
						</Header>
					</Grid>
				</MuiThemeProvider>
			</Router>
		</Provider>
	);
}

export default App;
