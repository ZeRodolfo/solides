import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import AuthActions from '../ducks/auth';
import ErrorActions from '../ducks/error';
import setAuthorizationHeader from '../../services/setAuthorizationHeader';

function saveUserToken(token) {
	setAuthorizationHeader(token);
	localStorage.setItem('@SOLIDES:userToken', token);
}

function readUserToken() {
	const token = localStorage.getItem('@SOLIDES:userToken');
	setAuthorizationHeader(token);
}

export function* login({ chapa, password }) {
	try {
		const { data } = yield call(api.post, '/auth/authenticate', {
			chapa,
			password
		});

		yield call(saveUserToken, data.token);

		yield put(AuthActions.loginSuccess(data.user));
	} catch (err) {
		yield put(AuthActions.loginFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* fetch() {
	try {
		yield call(readUserToken);
		const { data } = yield call(api.get, '/auth/authenticate');
		yield put(AuthActions.fetchUserSuccess(data));
	} catch (err) {
		yield put(AuthActions.fetchUserFailure());
		yield put(ErrorActions.show(err));
	}
}
