import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import PunchActions from '../ducks/punch';
import ErrorActions from '../ducks/error';
import history from '../../config/history';

function* putPunch(id, values) {
	const { data } = yield call(api.put, `/punches/${id}`, values);

	return data;
}

function* postPunch(values) {
	const { data } = yield call(api.post, '/punches', values);

	return data;
}

export function* save({ id, date, entry, leave, interval }) {
	try {
		const data =
			id !== undefined
				? yield putPunch(id, {
						date,
						entry,
						leave,
						interval
					})
				: yield postPunch({
						date,
						entry,
						leave,
						interval
					});

		yield put(PunchActions.punchSuccess(data));
		yield history.push('/punches');
	} catch (err) {
		yield put(PunchActions.punchFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* show({ id }) {
	try {
		if (id !== undefined) {
			const { data } = yield call(api.get, `/punches/${id}`);
			yield put(PunchActions.fetchPunchSuccess(data));
		}

		if (id === undefined) {
			const data = {
				date: '',
				entry: '',
				leave: '',
				interval: '',
				employee: null
			};

			yield put(PunchActions.fetchPunchSuccess(data));
		}
	} catch (err) {
		yield put(PunchActions.fetchPunchFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* list({ employee }) {
	try {
		console.log('oi', employee);
		if (employee !== null && employee !== undefined) {
			employee = `${employee}/employee`;

			const { data } = yield call(api.get, `/punches/${employee}`);
			yield put(PunchActions.listPunchesSuccess(data));
		}
		if (employee === undefined) {
			console.log('undefined');
			const { data } = yield call(api.get, `/punches`);
			yield put(PunchActions.listPunchesSuccess(data));
		}
	} catch (err) {
		yield put(PunchActions.listPunchesFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* destroy({ route }) {
	try {
		yield call(api.delete, route);
		const { data } = yield call(api.get, `/punches`);
		yield put(PunchActions.destroyPunchSuccess(data));
	} catch (err) {
		yield put(PunchActions.destroyPunchFailure());
		yield put(ErrorActions.show(err));
	}
}
