import { all, takeLatest } from 'redux-saga/effects';

import { AuthTypes } from '../ducks/auth';
import { EmployeeTypes } from '../ducks/employee';
import { PunchTypes } from '../ducks/punch';

import { login, fetch } from './auth';
import { 
	save as EmployeeSave, 
	show as EmployeeShow, 
	list as EmployeeList, 
	destroy as EmployeeDestroy
} from './employee';
import { 
	save as PunchSave, 
	show as PunchShow, 
	list as PunchList, 
	destroy as PunchDestroy
} from './punch';


export default function* rootSaga() {
	yield all([
		takeLatest(AuthTypes.LOGIN_REQUEST, login),
		takeLatest(AuthTypes.FETCH_USER_REQUEST, fetch),
		takeLatest(EmployeeTypes.EMPLOYEE_REQUEST, EmployeeSave),
		takeLatest(EmployeeTypes.FETCH_EMPLOYEE_REQUEST, EmployeeShow),
		takeLatest(EmployeeTypes.LIST_EMPLOYEES_REQUEST, EmployeeList),
		takeLatest(EmployeeTypes.DESTROY_EMPLOYEE_REQUEST, EmployeeDestroy),
		takeLatest(PunchTypes.PUNCH_REQUEST, PunchSave),
		takeLatest(PunchTypes.FETCH_PUNCH_REQUEST, PunchShow),
		takeLatest(PunchTypes.LIST_PUNCHES_REQUEST, PunchList),
		takeLatest(PunchTypes.DESTROY_PUNCH_REQUEST, PunchDestroy)
	]);
}
