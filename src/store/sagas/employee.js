import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import EmployeeActions from '../ducks/employee';
import ErrorActions from '../ducks/error';
import history from '../../config/history';

function* putEmployee(id, values) {
	const { data } = yield call(api.put, `/employees/${id}`, values);

	return data;
}

function* postEmployee(values) {
	const { data } = yield call(api.post, '/employees', values);

	return data;
}

export function* save({ id, chapa, name, password, role }) {
	try {
		const data =
			id !== undefined
				? yield putEmployee(id, {
						chapa,
						name,
						password,
						role
					})
				: yield postEmployee({
						chapa,
						name,
						password,
						role
					});

		yield put(EmployeeActions.employeeSuccess(data));
		yield history.push('/employees');
	} catch (err) {
		yield put(EmployeeActions.employeeFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* show({ id }) {
	try {
		if (id !== undefined) {
			const { data } = yield call(api.get, `/employees/${id}`);
			yield put(EmployeeActions.fetchEmployeeSuccess(data));
		}

		if (id === undefined) {
			const data = {
				chapa: '',
				name: '',
				password: '',
				role: 'estagiario'
			};

			yield put(EmployeeActions.fetchEmployeeSuccess(data));
		}
	} catch (err) {
		yield put(EmployeeActions.fetchEmployeeFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* list() {
	try {
		const { data } = yield call(api.get, '/employees');
		yield put(EmployeeActions.listEmployeesSuccess(data));
	} catch (err) {
		yield put(EmployeeActions.listEmployeesFailure());
		yield put(ErrorActions.show(err));
	}
}

export function* destroy({ route }) {
	try {
		yield call(api.delete, route);
		const { data } = yield call(api.get, `/employees`);
		yield put(EmployeeActions.destroyEmployeeSuccess(data));
	} catch (err) {
		yield put(EmployeeActions.destroyEmployeeFailure());
		yield put(ErrorActions.show(err));
	}
}
