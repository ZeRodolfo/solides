import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */
const { Types, Creators } = createActions(
	{
		punchRequest: [ 'id', 'date', 'entry', 'leave', 'interval' ],
		punchSuccess: [ 'punch' ],
		punchFailure: null,
		listPunchesRequest: [ 'employee' ],
		listPunchesSuccess: [ 'data' ],
		listPunchesFailure: null,
		fetchPunchRequest: [ 'id' ],
		fetchPunchSuccess: [ 'punch' ],
		fetchPunchFailure: null,
		destroyPunchRequest: [ 'route' ],
		destroyPunchSuccess: [ 'data' ],
		destroyPunchFailure: null
	},
	{ prefix: 'Punch/' }
);

export const PunchTypes = Types;
export default Creators;

/* Initial State */
export const INITIAL_STATE = Immutable({
	loading: false,
	date: '',
	entry: '',
	leave: '',
	interval: '',
	employee: null,
	data: []
});

/* Reducers */
export const reducer = createReducer(INITIAL_STATE, {
	[Types.PUNCH_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.PUNCH_SUCCESS]: (state, { punch }) => {
		return { ...state, loading: false, punch };
	},
	[Types.PUNCH_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.LIST_PUNCHES_REQUEST]: (state, { employee }) => {
		return { ...state, loading: true, employee };
	},
	[Types.LIST_PUNCHES_SUCCESS]: (state, { data }) => {
		return { ...state, loading: false, data };
	},
	[Types.LIST_PUNCHES_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.FETCH_PUNCH_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.FETCH_PUNCH_SUCCESS]: (state, { punch }) => {
		return {
			...state,
			loading: false,
			date: punch.date,
			entry: punch.entry,
			leave: punch.leave,
			interval: punch.interval,
			employee: punch.employee
		};
	},
	[Types.FETCH_PUNCH_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.DESTROY_PUNCH_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.DESTROY_PUNCH_SUCCESS]: (state, { data }) => {
		return {
			...state,
			loading: false,
			data
		};
	},
	[Types.DESTROY_PUNCH_FAILURE]: (state) => {
		return { ...state, loading: false };
	}
});
