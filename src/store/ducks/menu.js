import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */

const { Types, Creators } = createActions({
	menuClick: null
});

export const MenuTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
	open: false
});

/* Reducers */

export const reducer = createReducer(INITIAL_STATE, {
	[Types.MENU_CLICK]: (state) => state.merge({ open: !state.open })
});
