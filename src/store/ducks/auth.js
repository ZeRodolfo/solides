import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */
const { Types, Creators } = createActions(
	{
		loginRequest: [ 'chapa', 'password' ],
		loginSuccess: [ 'user' ],
		loginFailure: null,
		logout: null,
		fetchUserRequest: null,
		fetchUserSuccess: [ 'user' ],
		fetchUserFailure: null
	},
	{ prefix: 'Auth/' }
);

export const AuthTypes = Types;
export default Creators;

/* Initial State */
export const INITIAL_STATE = Immutable({
	user: null,
	loading: false
});

/* Reducers */
export const reducer = createReducer(INITIAL_STATE, {
	[Types.LOGIN_REQUEST]: (state) => state.merge({ loading: true }),
	[Types.LOGIN_SUCCESS]: (state, { user }) => state.merge({ loading: false, user }),
	[Types.LOGIN_FAILURE]: (state) => state.merge({ loading: false }),
	[Types.FETCH_USER_REQUEST]: (state) => state.merge({ loading: true }),
	[Types.FETCH_USER_SUCCESS]: (state, { user }) => state.merge({ loading: false, user }),
	[Types.FETCH_USER_FAILURE]: (state) => state.merge({ loading: false }),
	[Types.LOGOUT]: (state) => state.merge({ user: null })
});
