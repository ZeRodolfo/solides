import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */
const { Types, Creators } = createActions(
	{
		employeeRequest: [ 'id', 'chapa', 'name', 'password', 'role' ],
		employeeSuccess: [ 'user' ],
		employeeFailure: null,
		listEmployeesRequest: null,
		listEmployeesSuccess: [ 'data' ],
		listEmployeesFailure: null,
		fetchEmployeeRequest: [ 'id' ],
		fetchEmployeeSuccess: [ 'employee' ],
		fetchEmployeeFailure: null,
		destroyEmployeeRequest: [ 'route' ],
		destroyEmployeeSuccess: [ 'data' ],
		destroyEmployeeFailure: null
	},
	{ prefix: 'Employee/' }
);

export const EmployeeTypes = Types;
export default Creators;

/* Initial State */
export const INITIAL_STATE = Immutable({
	loading: false,
	chapa: '',
	name: '',
	password: '',
	role: 'estagiario',
	data: []
});

/* Reducers */
export const reducer = createReducer(INITIAL_STATE, {
	[Types.EMPLOYEE_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.EMPLOYEE_SUCCESS]: (state, { user }) => {
		return { ...state, loading: false, user };
	},
	[Types.EMPLOYEE_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.LIST_EMPLOYEES_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.LIST_EMPLOYEES_SUCCESS]: (state, { data }) => {
		return { ...state, loading: false, data };
	},
	[Types.LIST_EMPLOYEES_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.FETCH_EMPLOYEE_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.FETCH_EMPLOYEE_SUCCESS]: (state, { employee }) => {
		return {
			...state,
			loading: false,
			chapa: employee.chapa,
			name: employee.name,
			password: '',
			role: employee.role
		};
	},
	[Types.FETCH_EMPLOYEE_FAILURE]: (state) => {
		return { ...state, loading: false };
	},
	[Types.DESTROY_EMPLOYEE_REQUEST]: (state) => {
		return { ...state, loading: true };
	},
	[Types.DESTROY_EMPLOYEE_SUCCESS]: (state, { data }) => {
		return {
			...state,
			loading: false,
			data
		};
	},
	[Types.DESTROY_EMPLOYEE_FAILURE]: (state) => {
		return { ...state, loading: false };
	}
});
