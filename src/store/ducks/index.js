import { combineReducers } from 'redux';
import { reducer as auth } from './auth';
import { reducer as error } from './error';
import { reducer as menu } from './menu';
import { reducer as employee } from './employee';
import { reducer as punch } from './punch';

export default combineReducers({
	auth,
	error,
	menu,
	employee,
	punch
});
