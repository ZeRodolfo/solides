import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */
const { Types, Creators } = createActions(
	{
		show: [ 'error' ],
		hide: null
	},
	{ prefix: 'Error/' }
);

export const ErrorTypes = Types;
export default Creators;

/* Initial State */
export const INITIAL_STATE = Immutable({
	error: null
});

/* Reducers */
export const reducer = createReducer(INITIAL_STATE, {
	[Types.SHOW]: (state, { error }) => state.merge({ error }),
	[Types.HIDE]: (state) => state.merge({ error: null })
});
