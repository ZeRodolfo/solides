# Task Solides Tecnologia

Este projeto foi inspirado na Task da Solides Tecnologia.

## Sobre

Este projeto roda em conjunto com uma API criada especificamente para este projeto base.<br>
Repositório [Solides Tecnologia API](https://bitbucket.org/ZeRodolfo/solides-api).

## Funcionalidades

Este projeto é a realização de um desafio utilizando ReactJS.
Para a interface foi utilizada a biblioteca [Material-UI](https://http://material-ui.com).

O desafio tem como principal objetivo apontar as  hora de trabalho dos estagiários.


- Tela de login
- Tela de cadastro
- Controle de sessão via login
- Tela para apontar hora (chegada na empresa, hora de almoço, saída da empresa)
- Tela de histórico de apontamentos do usuário
- Design responsivo
- Sistema web, que roda em um navegador, com design responsivo voltado para celulares.

## Instalação

Para instalar os pacotes necessários basta executar o seguinte comando via terminal.<br>

`yarn install`

## Execução

Para a execução deste projeto será necessário executar o servidor (Backend) que irá prover e controlar toda as requisições realizadas pela aplicação.

O Repositório precisa ser executando em conjunto para funcionar corretamente.<br>

Para iniciar o projeto basta executar o seguinte comando via terminal.<br>

`yarn start`

## Usuário 

A aplicação irá permitir cadastrar dois tipos de usuários. 

- Administrador
- Estagiário

## Funcionalidades 

O usuário Administrador será responsável por cadastrar os outros usuários/funcionários. 
Onde o mesmo também irá poder ver o histórico de apontamentos realizados pelos funcionários. 

O usuário administrador só irá poder fazer seus próprios apontamentos e visualizar seu próprio histórico. 

## Acesso

- Administrador (chapa: 00001 senha: 123mudar)

